﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Vtb.Calc;

namespace Vtb
{
    public class TestCases
    {
        public List<Bonus> TestCalc(List<Transaction> t )
        {
            Calc c = new Calc(t);
            var responseList = c.CalculateBonus();

            return responseList.OrderBy(x => x.TransactionId).ToList();
        }


        public List<Transaction> GetTransactions()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-4)},
                new Transaction(){ From = "C", To = "D", Date = DateTime.Now.AddMinutes(-3)},
                new Transaction(){ From = "D", To = "F", Date = DateTime.Now.AddMinutes(-2)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-1)},
            };
        }

        public List<Transaction> FirstTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-5)},

            };
        }

        public List<Transaction> SecondTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-4)},
            };
        }

        public List<Transaction> ThirdTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "C", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-4)},
                new Transaction(){ From = "C", To = "D", Date = DateTime.Now.AddMinutes(-3)},
            };
        }

        public List<Transaction> ForthTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "D", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "B", To = "A", Date = DateTime.Now.AddMinutes(-4)},
                new Transaction(){ From = "A", To = "C", Date = DateTime.Now.AddMinutes(-3)},
            };
        }

        public List<Transaction> FifthTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-4)},
                new Transaction(){ From = "C", To = "D", Date = DateTime.Now.AddMinutes(-3)},
                new Transaction(){ From = "B", To = "F", Date = DateTime.Now.AddMinutes(-2)},
            };
        }

        public List<Transaction> SixthTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-4)},
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-3)},
                new Transaction(){ From = "A", To = "F", Date = DateTime.Now.AddMinutes(-2)},
            };
        }

        public List<Transaction> EightTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-10)},
                new Transaction(){ From = "C", To = "D", Date = DateTime.Now.AddMinutes(-9)},
                new Transaction(){ From = "G", To = "F", Date = DateTime.Now.AddMinutes(-8)},
                new Transaction(){ From = "B", To = "A", Date = DateTime.Now.AddMinutes(-7)},
                new Transaction(){ From = "F", To = "G", Date = DateTime.Now.AddMinutes(-6)},
                new Transaction(){ From = "B", To = "D", Date = DateTime.Now.AddMinutes(-5)},
                new Transaction(){ From = "A", To = "G", Date = DateTime.Now.AddMinutes(-4)},
                new Transaction(){ From = "F", To = "L", Date = DateTime.Now.AddMinutes(-3)},
                new Transaction(){ From = "B", To = "C", Date = DateTime.Now.AddMinutes(-2)},
            };
        }

        public List<Transaction> NinehtTest()
        {
            return new List<Transaction>() {
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-10)},
                new Transaction(){ From = "B", To = "A", Date = DateTime.Now.AddMinutes(-9)},
                new Transaction(){ From = "A", To = "B", Date = DateTime.Now.AddMinutes(-8)},
                new Transaction(){ From = "B", To = "A", Date = DateTime.Now.AddMinutes(-7)},
            };
        }

    }
}