﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtb
{
    public class Calc
    {
        private List<Transaction> _transactions;

        public Calc(List<Transaction> transactions)
        {
            _transactions = transactions;
        }
        public List<Bonus> CalculateBonus()
        {
            var bonusList = GetDistinctTransactionList();
            var array = new List<Matrix>();
            //List<string, List<string>>();

            for (int i = 0; i < _transactions.Count; i++)
            {
                //foreach (var item in _transactions)
                //{
                if (array.Count == 0)
                {
                    var matrix = new Matrix();
                    matrix.TransactionId = _transactions[i].From;
                    matrix.SecondTransactionId = _transactions[i].To;
                    // matrix.Nasted.Add(item.To);
                    array.Add(matrix);
                    continue;
                }


                var m = new Matrix();
                //if (!array.Any(x => x.TransactionId == item.From && x.SecondTransactionId == item.To))
                //{
                //if (!array.Any(x => x.TransactionId == item.To) && !array.Any(x => x.SecondTransactionId == item.To))
                //{
                m.TransactionId = _transactions[i].From;
                m.SecondTransactionId = _transactions[i].To;
                // m.Nasted.Add(item.To); 
                //}
                //else {
                //    continue;
                //}
                //  }

                if (!array.Any(x => x.TransactionId == _transactions[i].From && x.SecondTransactionId == _transactions[i].To))
                {
                    foreach (var ar in array)
                    {
                        var mainTr = array.Select(x => x.TransactionId).ToList();
                        var secondTr = array.Select(x => x.SecondTransactionId).ToList();

                        if (ar.SecondTransactionId == _transactions[i].From)
                        {
                            var firstIndex = mainTr.FindIndex(x => x == _transactions[i].From);
                            var secondIndex = secondTr.FindIndex(x => x == _transactions[i].From);
                            // var secondIndex = i;
                            if (firstIndex >= secondIndex || firstIndex == -1)
                            {
                                if (ar.TransactionId != _transactions[i].To)
                                {
                                    if (!secondTr.Contains(_transactions[i].To) && !mainTr.Contains(_transactions[i].To)){
                                        ar.Nasted.Add(_transactions[i].To);
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }

                array.Add(m);

            }


            foreach (var item in array)
            {
                if (String.IsNullOrEmpty(item.TransactionId) || String.IsNullOrEmpty(item.TransactionId))
                    continue;

                int point = item.Nasted.Count + 1;
                bonusList.First(x => x.TransactionId == item.TransactionId).Point = bonusList.First(x => x.TransactionId == item.TransactionId).Point + point;
            }

            return bonusList;
        }



        private List<Bonus> GetDistinctTransactionList()
        {
            var to = _transactions.Select(x => x.To);
            var from = _transactions.Select(x => x.From);
            var union = to.Union(from).ToList();

            return union.Distinct().Select(x => new Bonus() { Point = 0, TransactionId = x }).ToList();
        }



        public class Matrix
        {

            public Matrix()
            {
                Nasted = new List<string>();
            }
            public string TransactionId { get; set; }

            public string SecondTransactionId { get; set; }
            public List<string> Nasted { get; set; }
        }
        public class Bonus
        {
            public string TransactionId { get; set; }
            public int Point { get; set; }
        }
        public class Transaction
        {
            public string From { get; set; }

            public string To { get; set; }

            public DateTime Date { get; set; }
        }
    }
}