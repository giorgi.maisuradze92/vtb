﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtb
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new TestCases();

           var res  = test.TestCalc(test.EightTest());

            foreach (var item in res)
            {
                Console.WriteLine(item.Point + " " + item.TransactionId);
            }

            Console.ReadKey();
        }
    }
}
