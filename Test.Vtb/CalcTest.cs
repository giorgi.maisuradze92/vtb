﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vtb;
using static Vtb.Calc;
using System.Collections.Generic;

namespace Test.Vtb
{
    [TestClass]
    public class CalcTest
    {



        [TestMethod]
        public void TestFirst()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.FirstTest());


            var excepted = new List<Bonus>() {
                new Bonus(){ Point = 1, TransactionId = "A" },
                new Bonus(){ Point = 0, TransactionId = "B" },
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }


        [TestMethod]
        public void TestSecond()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.SecondTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 2, TransactionId = "A" },
                new Bonus(){ Point = 1, TransactionId = "B" },
                new Bonus(){ Point = 0, TransactionId = "C" }
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }



        [TestMethod]
        public void TestThird()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.ThirdTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 2, TransactionId = "A" },
                new Bonus(){ Point = 1, TransactionId = "B" },
                new Bonus(){ Point = 1, TransactionId = "C" },
                new Bonus(){ Point = 0, TransactionId = "D" },
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }

        [TestMethod]
        public void TestForth()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.ForthTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 2, TransactionId = "A" },
                new Bonus(){ Point = 1, TransactionId = "B" },
                new Bonus(){ Point = 0, TransactionId = "C" },
                new Bonus(){ Point = 0, TransactionId = "D" },
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }

        [TestMethod]
        public void TestFifth()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.FifthTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 3, TransactionId = "A" },
                new Bonus(){ Point = 3, TransactionId = "B" },
                new Bonus(){ Point = 1, TransactionId = "C" },
                new Bonus(){ Point = 0, TransactionId = "D" },
                new Bonus(){ Point = 0, TransactionId = "F" },
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }

        [TestMethod]
        public void TestSixth()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.SixthTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 4, TransactionId = "A" },
                new Bonus(){ Point = 1, TransactionId = "B" },
                new Bonus(){ Point = 0, TransactionId = "C" },
                new Bonus(){ Point = 0, TransactionId = "F" },
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }

        [TestMethod]
        public void TestNineth()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.NinehtTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 2, TransactionId = "A" },
                new Bonus(){ Point = 2, TransactionId = "B" },
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }

        [TestMethod]
        public void TestEighth()
        {
            var test = new TestCases();
            var res = test.TestCalc(test.EightTest());

            var excepted = new List<Bonus>() {

                new Bonus(){ Point = 2, TransactionId = "A" },
                new Bonus(){ Point = 3, TransactionId = "B" },
                 new Bonus(){ Point = 1, TransactionId = "C" },
                  new Bonus(){ Point = 0, TransactionId = "D" },
                   new Bonus(){ Point = 2, TransactionId = "F" },
                    new Bonus(){ Point = 2, TransactionId = "G" },
                     new Bonus(){ Point = 0, TransactionId = "L" }, 
            };
            CollectionAssert.AreEqual(res, excepted, new BonusComparer());
        }

    }

    public class BonusComparer : Comparer<Bonus>
    {
        public override int Compare(Bonus x, Bonus y)
        {
            int cardValue = x.TransactionId.CompareTo(y.TransactionId);
            int pointValue = x.Point.CompareTo(y.Point);
            return cardValue.CompareTo(pointValue);
        }
    }
}
